# SeLibra v1.3.14

## 介绍
基于php v8.0 + swoole v5.* 的高性能协程框架

### 软件架构
基于完整的PSR规范
<br>
1、php v8 +
<br>
2、swoole v5 +
<br>
3、基于 [Attribute] 的属性组件构建注解功能
<br>
4、基于 Psr\Container 的服务容器组件
<br>
5、自建依赖注入组件
<br>
6、基于 symfony/console 控制台管理组件


### 安装教程

1.  拉取仓库代码
2.  安装docker
3.  安装镜像及配置
4.  运行框架

### 使用说明
