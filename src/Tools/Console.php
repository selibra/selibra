<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Tools;


use Selibra\Files\Config;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Class Console
 * @package selibra
 */
class Console
{

    /**
     * 记录日志
     * @param $data
     */
    public static function log(...$data)
    {
        $debugBacktrace = debug_backtrace();
        $backtrace = reset($debugBacktrace);
        $backtraceArray = explode('/', $backtrace['file']);
        $file = end($backtraceArray);
        echo self::getDatetime() . ' # ';
        foreach ($data as $datum) {
            if (is_object($datum) || is_array($datum)) {
                ob_start();
                var_dump($datum);
                $datum = ob_get_clean();
            }
            echo $datum . '  ' . '[' . $file . '-' . $backtrace['line'] . 'L] ';
        }
        echo PHP_EOL;
    }

    /**
     * 记录日志
     * @param $data
     */
    public static function logNF(...$data)
    {
        $debugBacktrace = debug_backtrace();
        $backtrace = reset($debugBacktrace);
        $backtraceArray = explode('/', $backtrace['file']);
        $file = end($backtraceArray);
        echo self::getDatetime() . ' # ';
        foreach ($data as $datum) {
            if (is_object($datum) || is_array($datum)) {
                ob_start();
                var_dump($datum);
                $datum = ob_get_clean();
            }
            echo $datum;
        }
        echo PHP_EOL;
    }


    /**
     * @return mixed
     */
    public static function getDatetime()
    {
        $microtime = substr(microtime(), 2, 3);
        $date = date('Y-m-d H:i:s') . '.' . $microtime;
        return $date;
    }

}
