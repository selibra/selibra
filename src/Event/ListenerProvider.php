<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Event;


use Psr\EventDispatcher\ListenerProviderInterface;
use Selibra\Tools\Console;


/**
 * 事件提供者，负责返回给分发器当前事件的监听器
 * Class ListenerProvider
 * @package Selibra\Event
 */
class ListenerProvider implements ListenerProviderInterface
{

    /**
     * @inheritDoc
     * @return Listener[]
     */
    public function getListenersForEvent(object $event): iterable
    {
        $listenersArray = ListenerCollector::getListeners(get_class($event));
        if( empty($listenersArray) ){
            return [];
        }
        $listeners = [];
        foreach ($listenersArray as $item) {
            $listener = new Listener();
            $listener->setType($item['type']);
            $listener->setClass($item['class']);
            $listener->setMethod($item['method']);
            array_push($listeners, $listener);
        }
        return $listeners;
    }
}