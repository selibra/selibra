<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Event\Annotations;

use Selibra\Di\AnnotationConfigure;
use Selibra\Di\AnnotationExecEntity;
use Selibra\Di\Annotations\Protocol\SelibraAnnotationInterface;
use \Attribute;
use Selibra\Event\ListenerCollector;
use Selibra\Tools\Console;

/**
 * Class Listener
 * @package Selibra\Event\Annotations
 */
#[Attribute]
final class Listener implements SelibraAnnotationInterface
{

    protected string $eventClass;

    /**
     * Listener constructor.
     * @param $argument
     */
    public function __construct($eventClass)
    {
        $this->eventClass = $eventClass;
    }

    /**
     * @inheritDoc
     */
    public function exec(AnnotationExecEntity &$annotationExecEntity)
    {
        $type = \Selibra\Event\Listener::LISTENER_TYPE_CLASS;
        $method = '';
        if ($annotationExecEntity->hasMethod()) {
            $type = \Selibra\Event\Listener::LISTENER_TYPE_METHOD;
            $method = $annotationExecEntity->getMethod()->getName();
        }
        ListenerCollector::register($this->eventClass, $annotationExecEntity->getClass(), $method, $type);
    }

    /**
     * @inheritDoc
     */
    public function configure(AnnotationConfigure $annotationConfigure)
    {
        // TODO: Implement configure() method.
    }
}