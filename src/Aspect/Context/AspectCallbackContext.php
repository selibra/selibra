<?php
/*
 * @author eBIZ Team <dev@jungo.com.cn>
 * @copyright  Copyright (c) , ShenZhen JunGO Technology Co., Ltd. All rights reserved.
 * @license  Commercial authorization, unauthorized use and modification are prohibited
 * @url www.jungo.com.cn
 */
namespace Selibra\Aspect\Context;

use Selibra\Aspect\Intf\AspectCallbackContextInterface;

class AspectCallbackContext implements AspectCallbackContextInterface
{

    /**
     * 方法的返回值
     * @var mixed
     */
    protected mixed $functionReturnValue;

    /**
     * 错误
     * @var \Throwable|null
     */
    protected ?\Throwable $throwable;


    /**
     * AspectCallbackContext constructor.
     * @param array|null $functionParams 方法的参数
     */
    public function __construct(protected ?array $functionParams)
    {
    }


    /**
     * @param mixed $functionReturnValue
     * @return $this
     */
    public function setFunctionReturnValue(mixed $functionReturnValue): self
    {
        $this->functionReturnValue = $functionReturnValue;
        return $this;
    }

    /**
     * @param \Throwable|null $throwable
     * @return AspectCallbackContext
     */
    public function setThrowable(?\Throwable $throwable): AspectCallbackContext
    {
        $this->throwable = $throwable;
        return $this;
    }

    /**
     * 获取方法的参数
     *
     * @return array|null
     */
    public function getFunctionParams(): ?array
    {
        return $this->functionParams;
    }

    /**
     * 获取方法的返回值
     *
     * @return mixed
     */
    public function getFunctionReturnValue(): mixed
    {
        return $this->functionReturnValue;
    }

    /**
     * @return \Throwable|null
     */
    public function getFunctionThrow(): ?\Throwable
    {
        return $this->throwable;
    }
}
