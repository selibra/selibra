<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Aspect;


use application\Test\Before;
use Selibra\Tools\Console;

class AdviceCollector
{

    /**
     * @var array
     */
    protected static array $aspects = [];


    /**
     * @param string $namespace
     * @param $action
     * @param $aspectNamespace
     * @param $aspectAction
     * @param $type
     */
    public static function register(string $namespace, $action, $aspectNamespace, $aspectAction, $type)
    {
        self::$aspects[$namespace][$action][$type][] = [$aspectNamespace, $aspectAction];
    }


    /**
     * @param $class
     * @param $action
     * @return bool
     */
    public static function existAspect($class, $method = null)
    {
        if (!empty(self::$aspects[$class])) {
            return true;
        }
        if (!empty($method) && !empty(self::$aspects[$class][$method])) {
            return true;
        }
        return false;
    }


    /**
     * @param string $namespace
     * @param $action
     * @param $type
     * @return array
     */
    public static function getAspectData(string $namespace, $action, $type)
    {
        if (!array_key_exists($namespace, self::$aspects) || !array_key_exists($action, self::$aspects[$namespace])) {
            return null;
        }
        return self::$aspects[$namespace][$action][$type] ?? null;
    }

}
