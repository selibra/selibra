<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Aspect;


use Selibra\Di\DI;

class AspectObjectProxy
{
    /**
     * @var string
     */
    protected string $class = '';


    /**
     * @param $class
     * @return mixed
     * @throws \ReflectionException
     * @throws \Selibra\Di\Exception\NoImplementClass
     * @throws \Exception
     */
    public function getIns($class)
    {
        $this->class = $class;
        return (new ProxyObjectGenerator())->get($class);
    }




}