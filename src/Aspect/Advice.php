<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Aspect;


use Selibra\Aspect\Intf\AspectCallbackContextInterface;
use Selibra\Di\DI;

class Advice
{

    /**
     * @param $class
     * @param $action
     * @param $type
     * @param AspectCallbackContextInterface $aspectCallbackContext
     * @throws \Exception
     */
    public static function run($class, $action, $type, AspectCallbackContextInterface $aspectCallbackContext)
    {
        $aspects = AdviceCollector::getAspectData($class, $action, $type);
        if (!empty($aspects)) {
            foreach ($aspects as $aspect) {
                $aspectClass = $aspect[0];
                $aspectAction = $aspect[1];
                $aspectObject = DI::getObjectContext($aspectClass);
                call_user_func_array([$aspectObject, $aspectAction], [$aspectCallbackContext]);
            }
        }
    }

}
