<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Aspect;


use Selibra\Aspect\Events\InterfaceMethodTriggerEvent;
use Selibra\Event\EventTrigger;
use Selibra\Tools\Console;

class InterfaceMethodTrigger
{

    public function __construct(private string $namespace, private string $method)
    {
    }


    /**
     * 执行
     * @param mixed ...$arguments
     */
    public function run(...$arguments)
    {
        $event = new InterfaceMethodTriggerEvent($arguments, $this->namespace, $this->method);
        EventTrigger::listener($event);
        return $event->getResult();
    }

}
