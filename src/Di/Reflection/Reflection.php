<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Di\reflection;


use Selibra\Di\Container\Exception\MetadataNotFoundException;
use Selibra\Di\DI;
use Selibra\Tools\Console;

class Reflection
{

    /**
     * @var \ReflectionClass
     */
    protected \ReflectionClass $reflectClass;

    /**
     * Reflection constructor.
     * @param $id
     * @throws \ReflectionException
     */
    public function __construct($id)
    {
        $this->reflectClass = new \ReflectionClass($id);
    }


    /**
     * @return array
     */
    public function getConstructorParametersValues()
    {
        $constructor = $this->getReflectClass()->getConstructor();
        $constructorParametersValues = [];
        if (!empty($constructor)) {
            $constructorParameters = $constructor->getParameters();
            foreach ($constructorParameters as $constructorParameter) {
                if (!empty(@$constructorParameter->getClass())) {
                    try {
                        $constructorParametersValues[] = DI::getObjectContext(@$constructorParameter->getClass()->getName());
                    } catch (\ReflectionException $e) {
                    } catch (MetadataNotFoundException $e) {
                    }
                }
            }
        }
        return $constructorParametersValues;
    }

    /**
     * @return \ReflectionClass
     */
    public function getReflectClass(): \ReflectionClass
    {
        return $this->reflectClass;
    }
}
