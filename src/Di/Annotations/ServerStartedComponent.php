<?php
/**
 * Created by PhpStorm.
 * @author WYZ <wyz@jungo.com.cn>
 * @copyright 深圳市俊网网络有限公司
 */

namespace Selibra\Di\Annotations;


use Selibra\Di\Annotations\Protocol\ComponentAnnotationInterface;
use \Attribute;

/**
 * Class ServerStartedComponent
 * @package Selibra\Di\Annotations
 */
#[Attribute(Attribute::TARGET_CLASS)]
final class ServerStartedComponent implements ComponentAnnotationInterface
{

}