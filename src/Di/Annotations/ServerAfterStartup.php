<?php
/**
 * Created by PhpStorm.
 * @author WYZ <wyz@jungo.com.cn>
 * @copyright 深圳市俊网网络有限公司
 */

namespace Selibra\Di\Annotations;

use Selibra\Di\AnnotationConfigure;
use Selibra\Di\AnnotationExecEntity;
use Selibra\Di\Annotations\Protocol\SelibraAnnotationInterface;
use \Attribute;

/**
 * Class ServerAfterStartup
 * @package Selibra\Di\Annotations
 */
#[Attribute(Attribute::TARGET_METHOD)]
final class ServerAfterStartup implements SelibraAnnotationInterface
{

    /**
     * @var array
     */
    protected static array $startups = [];


    /**
     * @inheritDoc
     */
    public function exec(AnnotationExecEntity &$annotationExecEntity)
    {
        array_push(self::$startups,[$annotationExecEntity->getClass(),$annotationExecEntity->getMethod()->getName()]);
    }


    /**
     * @return array
     */
    public static function getStartups()
    {
        return self::$startups;
    }

    /**
     * @inheritDoc
     */
    public function configure(AnnotationConfigure $annotationConfigure)
    {
        // TODO: Implement configure() method.
    }
}