<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Di\DollarPhraseMapper;

use Selibra\Tools\Console;

class DollarPhraseMapperCollect
{

    private static array $mappers = [];

    /**
     * 添加映射
     * @param $key
     * @param $namespace
     */
    public static function addMapper($key, $namespace)
    {
        self::$mappers[$key] = $namespace;
    }


    /**
     * @param $key
     * @return mixed|null
     */
    public static function getMapper($key): ?string
    {
        if (!isset(self::$mappers[$key])) {
            return null;
        }
        return self::$mappers[$key];
    }


}
