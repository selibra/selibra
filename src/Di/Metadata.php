<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Di;


use Selibra\Di\Annotations\Protocol\SelibraAnnotationInterface;

class Metadata
{

    protected array $classAnnotations;


    protected array $methodsAnnotations;


    protected array $propertiesAnnotations;


    protected string $namespace;


    /**
     * @var \ReflectionClass
     */
    protected \ReflectionClass $reflectionClass;

    public function __construct($namespace)
    {
        $this->namespace = $namespace;
    }


    /**
     * @return string
     */
    public function getProxyClassName()
    {
        return DI::getClassProxyClassName($this->namespace);
    }


    /**
     * @return array<object>
     */
    public function getClassAnnotations(): array
    {
        return $this->classAnnotations;
    }

    /**
     * @param array $classAnnotations
     */
    public function setClassAnnotations(array $classAnnotations): void
    {
        $this->classAnnotations = $classAnnotations;
    }

    /**
     * @return array
     */
    public function getPropertiesAnnotations(): array
    {
        return $this->propertiesAnnotations;
    }


    /**
     * @param $property
     * @return SelibraAnnotationInterface[]|object[]
     */
    public function getPropertyAnnotations($property)
    {
        if( !array_key_exists($property,$this->propertiesAnnotations) ){
            return null;
        }
        return $this->propertiesAnnotations[$property];
    }

    /**
     * @param array $propertiesAnnotations
     */
    public function setPropertiesAnnotations(array $propertiesAnnotations): void
    {
        $this->propertiesAnnotations = $propertiesAnnotations;
    }

    /**
     * @return array
     */
    public function getMethodsAnnotations(): array
    {
        return $this->methodsAnnotations;
    }

    /**
     * @param array $methodsAnnotations
     */
    public function setMethodsAnnotations(array $methodsAnnotations): void
    {
        $this->methodsAnnotations = $methodsAnnotations;
    }

    /**
     * @return \ReflectionClass
     */
    public function getReflectionClass()
    {
        if (!isset($this->reflectionClass)) {
            return null;
        }
        return $this->reflectionClass;
    }

    /**
     * @param \ReflectionClass $reflectionClass
     */
    public function setReflectionClass(\ReflectionClass $reflectionClass): void
    {
        $this->reflectionClass = $reflectionClass;
    }

    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

}